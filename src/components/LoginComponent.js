import React, { Component } from 'react';
import { View, Image, TextInput, Keyboard, TouchableOpacity, Text } from 'react-native';
import { commonStyles } from "../styles/commonStyles"

export default class LoginComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: 'null'
        }
        this.onLogin = this.onLogin.bind(this);
    }
    handleChangeInputPassword = (passKey) => {
        this.setState({ password: passKey.nativeEvent.text })
    }
    onLogin() {
        if (this.state.password == "Grupee123") {
            this.props.navigation.navigate('viewProfile');
        } else {
            alert("Invalid Password");
        }
    }
    render() {

        return (
            <View style={commonStyles.mainView}>
                <View style={commonStyles.flexContainer}>
                    <View style={commonStyles.logoView}>
                        <Image
                            style={commonStyles.loginImage}
                            source={require("../assests/images/login.png")}
                        />
                        <TextInput style={commonStyles.loginInput}
                            type="password"
                            onEndEditing={Keyboard.dismiss}
                            value={this.state.passKey}
                            placeholder="Enter Password"
                            placeholderTextColor="#707070"
                            autoCapitalize="none"
                            secureTextEntry={true}
                            onChange={this.handleChangeInputPassword}
                        />
                        <TouchableOpacity
                            style={commonStyles.loginButtonStyle}
                            onPress={this.onLogin}
                        >
                            <Text style={commonStyles.loginButtonLabel}> Login </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}
