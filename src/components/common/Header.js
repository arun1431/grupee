
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Text, View, Image, TouchableOpacity, StyleSheet } from "react-native";

const Header = ({
    title,
    onBack,
    type
}) => {
    return (
        <View style={headerStyles.header}>
            {type != "viewprofile" && <TouchableOpacity
                style={headerStyles.headerNav}
                onPress={() => {
                    if (onBack) {
                        onBack();
                    }
                }}
            >
                <Image
                    source={require("../../assests/images/back_nav.png")}
                    style={headerStyles.headerNavIcon}
                />

            </TouchableOpacity>}
            <Text style={headerStyles.headerTitle}>
                {title}
            </Text>

        </View>
    );
};
Header.propTypes = {
    title: PropTypes.string,
    type: PropTypes.string,
    onBack: PropTypes.func
};
const headerStyles = StyleSheet.create({
    header: {
        height: 50,
        backgroundColor: "#0D1322",
        flexDirection: "row",
        justifyContent: "space-evenly",
        alignItems: "center",
        marginTop: 20
    },
    headerNav: {
        width: 50,
        height: 80,
        alignItems: "center",
        justifyContent: "center",
    },
    headerNavIcon: {
        width: 24,
        height: 24
    },
    headerTitle: {
        color: "#ffffff",
        fontSize: 18,
        fontWeight: "500",
        alignSelf: "center",
        flexGrow: 1,
        textAlign: "center",
    },


});
export default Header;