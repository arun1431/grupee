import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import { commonStyles } from "../styles/commonStyles"
import Header from "./common/Header"
import { TouchableOpacity } from 'react-native-gesture-handler';
import moment from "moment"
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'profileDatabase.db' });

export default class ViewProfileComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetching: false
        }

    }
    componentDidMount() {

        db.transaction(tx => {
            tx.executeSql(
                'create table if not exists profileList (url text, dateLiked);', [], () => console.log("creeeated"), (a, b) => console.log(b)
            );
        });
        this.props.actions.getProfile()
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps && nextProps.profileReducer && nextProps.profileReducer.profileLoaded && nextProps.profileReducer.profile) {
            this.setState({
                isFetching: true
            })
        }
    }
    onLikeClick(profileURL) {
        var date = moment().format()
        db.transaction(function (tx) {

            tx.executeSql(
                'INSERT INTO profileList (url,dateLiked) VALUES (?,?)',
                [profileURL, date],
                (tx, results) => {
                    console.log('Results', results.rowsAffected)
                }
            );
        });
        this.props.actions.getProfile()
        this.setState({
            isFetching: false
        })

    }
    onDislikeClick() {
        this.props.actions.getProfile()
        this.setState({
            isFetching: false
        })
    }
    render() {
        return (
            <View style={commonStyles.mainView}>
                <Header title={"Dog For You"} type={"viewprofile"} />
                {this.state.isFetching == true ? (<View style={commonStyles.logoView}>
                    <View style={commonStyles.flexContainer}>
                        <Image
                            style={commonStyles.profileImage}
                            source={
                                { uri: this.props.profileReducer.profile.message }
                            }

                        />

                        <View style={{ marginTop: 30, flexDirection: "row", justifyContent: "space-between", paddingHorizontal: 30 }}>
                            <TouchableOpacity onPress={() => {
                                this.onLikeClick(this.props.profileReducer.profile.message)
                            }}>
                                <View style={commonStyles.likeView}>
                                    <Image
                                        style={commonStyles.likeImgView}
                                        source={require("../assests/images/like.png")}
                                    />
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => {
                                this.onDislikeClick()
                            }}>
                                <View style={commonStyles.likeView}>
                                    <Image
                                        style={commonStyles.likeImgView}
                                        source={require("../assests/images/dislike.png")}

                                    />
                                </View>
                            </TouchableOpacity>

                        </View>
                        <TouchableOpacity
                            style={commonStyles.loginButtonStyle}
                            onPress={() => {
                                this.props.navigation.navigate('profileList')
                            }}
                        >
                            <Text style={commonStyles.loginButtonLabel}> View Liked Profiles </Text>
                        </TouchableOpacity>
                    </View>
                </View>) : (<View
                    style={{
                        flex: 1,
                        alignItems: "center",
                        justifyContent: "center"
                    }}
                >
                    <Text
                        style={{
                            fontSize: 14,
                            letterSpacing: 1,
                            color: "#FFFFFF",
                            marginTop: 15
                        }}
                    >
                        Loading
                        </Text>
                </View>
                    )}

            </View>
        );
    }
}
