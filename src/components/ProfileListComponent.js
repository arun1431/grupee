import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { commonStyles } from "../styles/commonStyles"
import Header from "./common/Header"
import moment from "moment"
import { ScrollView } from 'react-native-gesture-handler';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'profileDatabase.db' });
import map from "lodash/map";

export default class ProfileListComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profileListItems: [],
        };
        db.transaction(tx => {
            tx.executeSql('SELECT * FROM profileList', [], (tx, results) => {
                var temp = [];
                for (let i = 0; i < results.rows.length; ++i) {
                    temp.push(results.rows.item(i));
                }
                this.setState({
                    profileListItems: temp,
                });
            });
        });
    }
    render() {

        return (
            <View style={commonStyles.mainView}>
                <Header title={"Dog Wish List"}
                    onBack={() => {
                        this.props.navigation.goBack();

                    }} />
                <View style={[commonStyles.flexContainer, { flex: 1, marginTop: 10 }]}>
                    <ScrollView>
                        {map(this.state.profileListItems, profile => {
                            return (<View style={commonStyles.profileList} >
                                <View style={commonStyles.profileItem}>
                                    <View style={commonStyles.profileItemImgView}>
                                        <Image style={commonStyles.profileItemImg}
                                            source={
                                                { uri: profile.url }
                                            }
                                        />
                                    </View>
                                    <View style={commonStyles.profileItemContent}>
                                        <Text style={{ color: "#000", fontWeight: "400" }}>{'Liked At'}</Text>
                                        <Text style={{ color: "#000", fontWeight: "800" }}>{moment(profile.dateLiked).format('MMMM Do YYYY, h:mm:ss a')}</Text>
                                    </View>
                                </View>

                            </View>)
                        })}
                    </ScrollView>
                </View>
            </View >
        );
    }
}
