import { StyleSheet } from "react-native";


export const commonStyles = StyleSheet.create({
    mainView: {
        flex: 1,
        backgroundColor: "#151E36"
    },
    flexContainer: {
        flexGrow: 1,
        paddingHorizontal: 3
    },
    logoView: {
        flex: 1,
        alignItems: 'center'
    },
    loginImage: {
        height: 90,
        width: 200,
        marginTop: 150,
        alignSelf: "center"
    },
    loginInput: {
        margin: 15,
        height: 40,
        width: 260,
        borderColor: '#0041FF',
        borderWidth: 1,
        textAlign: 'center',
        borderRadius: 5,
        color: "#FFF",
        marginTop: 20
    },
    loginContent: {
        justifyContent: "center",
        alignItems: "center",
        marginTop: 20
    },
    loginButtonStyle: {
        height: 40,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#0041FF",
        paddingHorizontal: 50,
        paddingVertical: 5,
        marginTop: 15
    },
    loginButtonLabel: {
        color: "#FFF",
        fontSize: 20
    },
    profileList: {
        paddingHorizontal: 10,
        paddingVertical: 10,
        flexGrow: 1,
    },
    profileItem: {
        backgroundColor: "#FFF",
        width: "100%",
        flexDirection: "row",
        paddingHorizontal: 20,
        paddingVertical: 10,
        borderRadius: 10,
        marginTop: 1
    },
    profileItemImgView: {
        width: 40,
        alignSelf: "center",
    },
    profileItemImg: {
        width: 80,
        height: 80,
    },
    profileItemContent: {
        flexGrow: 1,
        alignItems: "center",
        justifyContent: "center",
        marginLeft: 40
    },
    profileItemDate: {
        color: "rgba(255,255,255,0.6)",
        alignSelf: "center",
        fontSize: 12
    },
    likeView: {
        borderColor: "#000",
        borderWidth: 1,
        borderRadius: 40,
        height: 90,
        width: 90,
        backgroundColor: "#FFF",
        justifyContent: "center",
        alignItems: "center"
    },
    likeImgView: {
        height: 65,
        width: 65,
        alignSelf: "center"
    },
    profileImage: {
        height: 300,
        width: 300,
        marginTop: 100,
        alignSelf: "center",
        borderColor: "#fff",
        borderWidth: 1
    }
});
