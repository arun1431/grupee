import {
    createStackNavigator,
    createAppContainer
} from "react-navigation";
import LoginComponent from "./components/LoginComponent";
import ProfileListComponent from "./components/ProfileListComponent";
import ViewProfileContainer from "./container/ViewProfileContainer";
RootNavigator = createStackNavigator({
    Login: {
        screen: LoginComponent,
        navigationOptions: {
            header: null
        }
    },
    profileList: {
        screen: ProfileListComponent,
        navigationOptions: {
            header: null,
        }
    },
    viewProfile: {
        screen: ViewProfileContainer,
        navigationOptions: {
            header: null
        }
    },
    initialRouteName: "Login"
});

const AppContainer = createAppContainer(RootNavigator);
export default AppContainer;