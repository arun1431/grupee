import { combineReducers } from "redux";
import profileReducer from "./profileReducer";


const appReducer = combineReducers({
    profileReducer
});

const rootReducer = (state, action) => {

    return appReducer(state, action);

};
export default rootReducer;
