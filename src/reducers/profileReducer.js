export const GET_PROFILE = "GET_PROFILE";
const initialState = {
    profile: {},
    profileLoaded: false
}


export default function profileReducer(state = initialState, action) {
    switch (action.type) {

        case GET_PROFILE: {
            if (action.unableToFetch) {
                return Object.assign({}, state, { profileLoaded: false });
            } else {
                return Object.assign({}, state, { profile: action.profile, profileLoaded: true });
            }
        }
        default:
            return state;

    }

}