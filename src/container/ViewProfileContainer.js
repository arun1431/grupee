import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { ActionCreators } from "../actions/index";
import ViewProfileComponent from "../components/ViewProfileComponent"



class ViewProfileContainer extends React.Component {
    render() {
        return <ViewProfileComponent {...this.props} />
    }
}
function mapStateToProps(state) {
    return {
        profileReducer: state.profileReducer
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(ActionCreators, dispatch)
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ViewProfileContainer)