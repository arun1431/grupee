import axios from "axios";
import * as types from "../constants/actionTypes";
import * as API_END_POINTS from "../constants/api";
export function getProfile() {

    return dispatch => {
        axios
            .get(API_END_POINTS.GET_PROFILE)
            .then(function (response) {
                if (response.status == 200) {

                    dispatch({
                        type: types.GET_PROFILE,
                        profile: response.data,
                    });
                } else {
                    alert("Response Error");
                    dispatch({
                        type: types.GET_PROFILE,
                        unableToFetch: true
                    });
                }
            })
            .catch(err => {
                alert("Error check for internet status");
                if (err && err.config) {
                    dispatch({
                        type: types.GET_PROFILE,
                        unableToFetch: true
                    });
                }
            });
    };
}